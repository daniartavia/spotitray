#!/bin/bash

echo "Eliminando archivo .py"
rm $HOME/.spotitray.py

echo "Eliminando íconos"
rm $HOME/.config/spotify/spotilogo.png

echo "Eliminando entrada en el menú"
rm $HOME/.local/share/applications/spotify.desktop