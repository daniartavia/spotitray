#!/bin/bash

#Verifica las dependencias
command -v spotify >/dev/null 2>&1 || { echo "El paquete 'spotify' es requerido y no se ha detectado en el sistema. Saliendo" >&2; exit 1; }
command -v wmctrl >/dev/null 2>&1 || { echo "El paquete 'wmctrl' es requerido y no se ha detectado en el sistema. Saliendo" >&2; exit 1; }
command -v xdotool >/dev/null 2>&1 || { echo "El paquete 'xdotool' es requerido y no se ha detectado en el sistema. Saliendo" >&2; exit 1; }

echo "Copiando archivo .py"
cp spotitray.py $HOME/.spotitray.py

echo "Copiando íconos"
cp spotilogo.png $HOME/.config/spotify/spotilogo.png

echo "Creando entrada en el menú"
cp spotify.desktop $HOME/.local/share/applications/spotify.desktop
echo "Para usar la entrada predeterminada de Spotify:"
echo "  mv $HOME/.local/share/applications/spotify.desktop $HOME/.local/share/applications/spotify2.desktop"