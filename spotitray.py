#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    ** INICIO DE LICENCIA **
#
#    Spotitray - a tray icon for Spotify
#    Copyright (C) 2016  Daniel Artavia
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    ** FIN DE LICENCIA **
#
#    Spotitray  Copyright (C) 2016  Daniel Artavia
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#
# Dependencias:
# -spotify
# -xdotool
# -wmctrl
# -Gtk (3.0+)
# -libnotify


import os
import re
import subprocess as cmd_output
import sys
import time

import dbus
from dbus import Interface
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Notify


class Spotify():
	pidfile = "/tmp/spotipid"
	session_bus = None
	player = None
	playing = None
	metadata = None
	proxy = None
	notif = None

	#Recibe señales y controla el reproductor
	def event_handler(self, signal):
		if signal == 1:
			self.player.PlayPause()
		elif signal == 2:
			self.player.Next()
		elif signal == 3:
			self.player.Previous()
			self.player.Previous()
		elif signal == 4:
			self.player.Previous()

	#Escucha por eventos constantemente
	def event_listener(self):
		#Si falla en obtener la metadata, sale.
		try:
			track = self.get_metadata()
		except Exception:
			os.system("rm " + self.pidfile)
			sys.exit(0)

		#Actualiza la notificación con el cambio de canción
		if track[0] != self.metadata[0]:
			self.metadata = self.get_metadata()
			self.notification()

		#Retorna True para mantener el ciclo corriendo
		return True

	#Obtiene la imagen de la canción actual.
	def get_art(self, url):
		art_folder = os.environ['HOME'] + "/.cache/spotify/Arts/"

		#Crea el folder si no existe
		if not os.path.exists(art_folder):
			os.system("mkdir " + art_folder)

		art_id = url.split("image/")
		art_id = art_id[1]
		if not os.path.exists(art_folder + art_id):
			os.system("cd " + art_folder + " && wget -q " + url)

		return art_folder + art_id

	#Obtiene la metdata de la canción actual.
	def get_metadata(self):
		try:
			iface = dbus.Interface(self.proxy, "org.freedesktop.DBus.Properties")
			metadata = iface.Get("org.mpris.MediaPlayer2.Player", "Metadata")

			artist=""
			for i in metadata['xesam:artist']:
				artist = i
			title = metadata['xesam:title']
			album = metadata['xesam:album']
			art = metadata['mpris:artUrl']
			ret = [title, artist, album, art]
			
			return ret
		except Exception:
			os.system("rm " + self.pidfile)
			sys.exit(0)

	#Retorna la interfaz para controlar el navegador
	def get_player(self):
		proxy = self.session_bus.get_object("org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2")
		iface = dbus.Interface(proxy, "org.mpris.MediaPlayer2.Player")
		return iface

	#Obtiene el estado del reproductor (playing o paused)
	def get_status(self):
		try:
			iface = dbus.Interface(self.proxy, "org.freedesktop.DBus.Properties")
			status = iface.Get("org.mpris.MediaPlayer2.Player", "PlaybackStatus")
			return status
		except Exception:
			return False

	#Muestra una notificación con la metadata de la canción
	def notification(self):
		album_art = self.get_art(self.metadata[3])
		Notify.init("Spotify")

		#Cierra las notificaciones existentes
		if self.notif != None:
			self.notif.close()

		self.notif = Notify.Notification.new(self.metadata[0], self.metadata[1] + " - " + self.metadata[2], album_art)
		self.notif.show()

	def __init__(self, daemon=True):
		self.loop = GObject.MainLoop()
		self.session_bus = dbus.SessionBus()
		self.proxy = self.session_bus.get_object("org.mpris.MediaPlayer2.spotify", "/org/mpris/MediaPlayer2")
		self.daemon = daemon
		self.metadata = self.get_metadata()
		self.player = self.get_player()
		self.playing = None
		
		#Si daemon es True se ejecuta como demonio.
		if daemon == True:
			if os.path.exists(self.pidfile):
				existing_pid = open(self.pidfile).read().strip();
				actual_pid = str(os.getpid())
				#Si está corriendo, cierra el proceso anterior
				if existing_pid != actual_pid:
					os.system("kill " + existing_pid)
					os.system("rm " + self.pidfile)
					os.system("echo " + actual_pid + " > " + self.pidfile)
			else:
				actual_pid = str(os.getpid())
				os.system("echo " + actual_pid + " > " + self.pidfile)

			#Inicia el MainLoop de GObject
			GObject.timeout_add(100, self.event_listener)
			self.loop.run()
		else:
			#Si daemon no es True envía la señal recibida a event_handler()
			self.event_handler(daemon)


class TrayIcon(Gtk.StatusIcon):
	player = None
	pidfile = "/tmp/spotipid"
	hidden = False
	metadata = None
	playing = None
	notification = None

	#Recibe señales del menú y las envía a la clase Spotify
	def connect_handler(self, widget, b, data):
		if data == 1:
			self.player.event_handler(1)
		elif data == 2:
			self.player.event_handler(3)
		elif data == 3:
			self.player.event_handler(2)

	#Saleale del script
	def exit(self, widget, c=""):
		os.system("rm " + self.pidfile)
		os.system("pkill spotify")
		sys.exit(0)

	#Muestra el menú con el click derecho
	def show_menu(self, status, button, time):
		self.menu.popup(None, None, None, None, button, time)

	#Con el click izquierdo, muestra u oculta la ventana principal
	def toggle_main_window(self, widget):
		status = self.player.get_status()
		if status == "Paused":
			#Si está pausado controla la ventana Spotify
			window_id = get = os.popen("wmctrl -G -l | grep Spotify").read().split()
			window_id = window_id[0]
			os.system("xdotool windowminimize " + window_id)
			os.system("wmctrl -i -r " + window_id + " -b toggle,skip_taskbar")
			if self.hidden == False:
				self.hidden = True
			else:
				os.system("wmctrl -i -a " + window_id)
				self.hidden = False
		else:
			#Si no está pausado controla la ventana según el título de la canción actual
			metadata = self.player.get_metadata()
			title = str(metadata[1] + " - " + metadata[0])
			window_id = os.popen("wmctrl -G -l | grep '" + title + "'").read().split()
			os.system("xdotool windowminimize "+window_id[0])
			os.system("wmctrl -i -r " + window_id[0] + " -b toggle,skip_taskbar")
			if self.hidden == False:
				self.hidden = True
			else:
				os.system("wmctrl -i -a " + window_id[0])
				self.hidden = False

	#Actualiza la canción en el menú
	def update_song(self):
		if self.player.get_status() != False:
			metadata = self.player.get_metadata()
			self.metadata = metadata
			self.playing = self.player.get_status()
			return True
		else:
			sys.exit(0)

	def make_notif(self, a=None, b=None, c=None):
		if self.playing == "Paused":
			playpause = "Reproducir"
		else:
			playpause = "Pausa"
		if self.notification != None:
			self.notification.close()
		art_id = self.metadata[3].split("image/")
		art = os.environ['HOME'] + "/.cache/spotify/Arts/" + art_id[1]
		Notify.init("Spotitray_control")
		self.notification = Notify.Notification.new(self.metadata[0], self.metadata[1] + " - " + self.metadata[2], art)
		self.notification.set_timeout(5000)
		self.notification.add_action("Play", playpause, self.connect_handler, 1)
		self.notification.add_action("Anterior", "Anterior", self.connect_handler, 2)
		self.notification.add_action("Siguiente", "Siguiente", self.connect_handler, 3)
		self.notification.add_action("Salir", "Salir", self.exit)
		self.notification.show()

	def printer(self,a="a",b="",c=""):
		print(c)

	def __init__(self):
		#Si spotify no está corriendo, sale.
		spotify_running = os.popen("pidof spotify").read()
		if len(spotify_running) != 0:
			dialog = Gtk.MessageDialog(title="Error", buttons=Gtk.ButtonsType.OK)
			dialog.set_markup("Spotify ya está corriendo.")
			dialog.run()
			sys.exit(0)
		else:
			os.system("spotify > /dev/null 2>&1 &")
			time.sleep(4)

		#Obtiene una instancia de la clase Spotify. No como servicio.
		self.player = Spotify(None)

		Gtk.StatusIcon.__init__(self)
		self.set_from_file(os.environ['HOME']+"/.config/spotify/spotilogo.png")
		self.set_tooltip_text("Spotitray")
		self.set_visible(True)

		self.connect("activate", self.toggle_main_window)
		self.connect('popup-menu', self.make_notif)

		GLib.timeout_add(1500,self.update_song)

if __name__ == "__main__":
	try:
		ti = TrayIcon()
		s = Spotify()
		Gtk.main()
	except KeyboardInterrupt:
		print("Señal de salida recibida. Saliendo")
		os.system("rm " + self.pidfile)
		sys.exit(0)
		